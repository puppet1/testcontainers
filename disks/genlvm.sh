#!/usr/bin/env bash
set -eux
export LVM_VG_NAME=vms_vg

echo "This will not recreate existing volume group, if you want that, regenerate the disks"

# detach lo devices
vgchange -a n vms_vg || true
losetup -D || true

function genlvm () {
  lodev=`losetup --find --nooverlap --show ./$1`
  echo "dev: $lodev"
  fdisk -l $lodev
  pvcreate $lodev || true
  vgcreate vms_vg $lodev || true
  vgchange -a n vms_vg
  losetup -d $lodev
}

genlvm ./hv2.img
genlvm ./hv1.img

