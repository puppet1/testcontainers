#!/usr/bin/env bash

function genimg () {
  if ls ./$1.img
  then
    echo already exist
    echo -ne "  real: "
    du -h $1.img
    echo -ne "  apparent: "
    du -h --apparent-size $1.img

  else
   truncate -s 100G $1.img
  fi
}

genimg hv1
genimg hv2
