#!/usr/bin/env bash

lohv1=`losetup --find --nooverlap --show ./hv1.img`
lohv2=`losetup --find --nooverlap --show ./hv2.img`

echo $lohv1 $lohv2

losetup -d $lohv1
losetup -d $lohv2

