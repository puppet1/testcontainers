FROM debian:buster

RUN apt-get update
RUN apt-get -y upgrade

RUN apt-get -y install bind9utils curl git openssh-client python-jinja2 python-six python-yaml python3-pip python3-setuptools less vim screen golang vde2 sshfs openssh-sftp-server


# ansible complains if it can't look up uid in /etc/passwd
#RUN adduser --uid 1000 --disabled-login user0 --no-create-home
#RUN adduser --uid 1001 --disabled-login user1 --no-create-home

RUN mkdir /home/user
RUN chmod a+rwx /home/user

RUN useradd  -d /home/user -s /bin/bash --uid 1000 user0 
RUN useradd  -d /home/user -s /bin/bash --uid 1001 user1



COPY build-float-runner.sh /tmp/build.sh
RUN /tmp/build.sh && rm /tmp/build.sh
COPY float-with-ssh-key /usr/bin/float-with-ssh-key
COPY with-ssh-key /usr/bin/with-ssh-key
COPY reverse-sshfs /usr/bin/reverse-sshfs 
#https://blog.dhampir.no/content/reverse-sshfs-mounts-fs-push
#ENTRYPOINT /usr/bin/float-with-ssh-key
ENTRYPOINT /usr/bin/with-ssh-key

