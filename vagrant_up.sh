#!/usr/bin/env bash

set -eux

export VAGRANT_EXPERIMENTAL="disks"

vagrant up --provider=libvirt
