#!/bin/sh
#
# Install script for apache2-users inside a Docker container.
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="
	golang
"

MITOGEN_VERSION="0.2.9"
MITOGEN_URL="https://networkgenomics.com/try/mitogen-${MITOGEN_VERSION}.tar.gz"

GO_PKGS="
	git.autistici.org/ale/x509ca
	git.autistici.org/ale/ed25519gen
"

set -x
set -e
umask 022

pip3 install wheel
# use ansible from pip, so hostvars behaves as a dict
pip3 install ansible jmespath netaddr

# Download and install Mitogen.
(cd /usr/src ; \
 curl -sL ${MITOGEN_URL} | tar xzf - ; \
 chown -R root.root mitogen-${MITOGEN_VERSION})
echo "export MITOGEN=/usr/src/mitogen-${MITOGEN_VERSION}" \
    > /etc/profile.d/mitogen.sh

# Install Go packages.
for pkg in $GO_PKGS; do
    go get ${pkg}
done
mv $HOME/go/bin/* /usr/bin/ 

# Ensure our wrapper is executable.
#chmod 755 /usr/bin/with-ssh-key

rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
