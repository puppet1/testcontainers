#!/usr/bin/env bash


mkdir -p /dev/block
for i in /sys/block/*/dev /sys/block/*/*/dev
do
  if [ -f $i ]
  then
    MAJOR=$(sed 's/:.*//' < $i)
    MINOR=$(sed 's/.*://' < $i)
    DEVNAME=$(echo $i | sed -e 's@/dev@@' -e 's@.*/@@')
    #echo mknod /dev/$DEVNAME b $MAJOR $MINOR
    if ls /dev/$DEVNAME
    then
      echo mknod /dev/block/${MAJOR}:${MINOR}
      mknod /dev/block/${MAJOR}:${MINOR} b $MAJOR $MINOR
    fi

#/dev/block/7:0
  fi
done

exit 0
  # Populate char devices

  for i in /sys/bus/*/devices/*/dev /sys/class/*/*/dev
  do
    if [ -f $i ]
    then
      MAJOR=$(sed 's/:.*//' < $i)
      MINOR=$(sed 's/.*://' < $i)
      DEVNAME=$(echo $i | sed -e 's@/dev@@' -e 's@.*/@@')
      echo mknod /dev/$DEVNAME c $MAJOR $MINOR
    fi
  done

vgchange -ay
