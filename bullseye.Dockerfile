FROM debian:bullseye

ENV container docker
ENV LC_ALL C
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install -y systemd systemd-sysv ssh
#    && apt-get clean \
#    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN ls -lhtr /lib/systemd/system/*.wants/*

#/lib/systemd/system/multi-user.target.wants/* \
#    /etc/systemd/system/*.wants/* \

RUN rm -f /lib/systemd/system/local-fs.target.wants/* \
    /lib/systemd/system/sockets.target.wants/*udev* \
    /lib/systemd/system/sockets.target.wants/*initctl* \
    /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* \
    /lib/systemd/system/systemd-update-utmp*

COPY sshd_config /etc/ssh/sshd_config

RUN rm /etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_ecdsa_key.pub /etc/ssh/ssh_host_ed25519_key /etc/ssh/ssh_host_ed25519_key.pub /etc/ssh/ssh_host_rsa_key /etc/ssh/ssh_host_rsa_key.pub

#COPY ssh-keygen.service /etc/systemd/system/ssh-keygen.service
#RUN systemctl enable ssh-keygen.service
RUN mkdir /root/.ssh
RUN apt-get install -y python3 python python3-pip curl wget

COPY sshkeys/authorized_keys.docker /root/.ssh/authorized_keys
VOLUME [ "/sys/fs/cgroup" ]

CMD ["/lib/systemd/systemd"]
