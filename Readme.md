# docker containers to test libvirt / lvm / puppet stuff

If you can write to the repo's, replace all http url's with the ssh equivalents.

### install the stuff you need on your computer

apt install docker docker-compose git sshfs openssh-sftp-server

make sure you have a ssh and gpg key 

### install the new server with
- debian 10
- ssh with key configured
- apt install sshfs

### setup the puppet code repo + testcontainers

Create a directory "automation"

```
cd automation

git clone https://git.puscii.nl/puppet1/testcontainers.git
mkdir puppet
cd puppet
mkdir scripts 
mkdir private_modules # put the modules under development / not for publication here
git clone https://git.puscii.nl/puppet1/nomasters.git
mv nomasters code
cd code
./init-submodules.sh
```

#### create ssh keys for testing

```
cd testcontainers/sshkeys
echo "your-ssh-key" > my.key 
./genkeys.sh
```

#### create disk images for testing

```
cd testcontainers/disks
./gendisks.sh
sudo ./genlvm.sh
sudo ./mount.sh
```

Then check what loop devices they are mounted on and update testcontainers/docker-compose.yml

#### build and start the containers that will run libvirt / kvm

```
cd testcontainers
docker-compose up --build
```

#### check if it works

```
ssh root@172.28.5.11
ssh root@172.28.5.12
```

The puppet directory should now be mounted on /etc/puppet and you should be able to test you puppet modules.

### initializing secrets / hiera / ssl keys using float

Either create a new private repository, use an existing one, or use the one with testdata

To make a new one:

```

mkdir private
diceware -n 14 > private/ansible-password
git clone https://git.puscii.nl/puppet1/float.git
git clone https://git.puscii.nl/puppet1/float-minimal.git

```

create private/hosts.yml:

```
---
group_vars:
  hypervisor: {ansible_become: false, ansible_ssh_private_key_file: ../testcontainers/sshkeys/admin,
            ansible_user: root}
  all: 
    admins:
      - email: admin@example.com
        name: admin
        password: $s$16384$8$1$c479e8eb722f1b071efea7826ccf9c20$96d63ebed0c64afb746026f56f71b2a1f8796c73141d2d6b1958d4ea26c60a0b
        ssh_keys: 
              - "ssh-ed25519 [key here] admin"
        gpg_keys:
              - "gpg fingerprint here"

    networks:
      external:
        network: 192.168.185.0/24
        gateway: 192.168.185.1
        netmask: 255.255.255.0
        dns: 8.8.8.8
      net-internal:
        network: 192.168.23.0/24
        gateway: 192.168.23.1
        netmask: 255.255.255.0
        dns: 1.1.1.1.1
    

hosts:
  hv1:
    ansible_host: 172.28.5.11
    groups: [frontend, backend, hypervisor]
    ip: 172.28.5.11
    name: hv1
    puppetroles:
      - role::hypervisor
  hv2:
    ansible_host: 172.28.5.12
    groups: [hypervisor]
    ip: 172.28.5.12
    name: hv2
    puppetroles:
      - role::hypervisor

  testhost:
    #ansible_host: 172.28.5.13
    groups: [vm]
    hypervisor: hv1
    #ip: 172.28.5.13
    name: testhost
    internal_host: 13
    internal_net: net-internal
    external_host: 13
    external_net: external
 
  testtwee:
    #ansible_host: 192.168.23.167
    #ip: 192.168.23.167
    internal_host: 167
    internal_net: net-internal
    external_host: 167
    external_net: external
    groups: [vm]
    hypervisor: hv1
    rootsize: '2G'
    internal_tcp_services: [22,443]
    internal_custom_tcp_rules: 
      - '192.168.23.166': '80'
    #external_tcp_services: [ 443 ]
  testhost1:
    ansible_host: 172.28.5.13
    groups: [vm]
    hypervisor: hv2
    #ip: 172.28.5.13
    name: testhost1
    internal_host: 2
    internal_net: net-internal
    external_host: 2
    external_net: external

```

### setup float 

Either use the docker image that has the stuff setup: 

```
float-minimal/ssh.sh (this will start a shell in container with the tools installed and testkey loaded in the agent)
```

Or install the things on your local system:

```
apt install bind9utils curl git openssh-client python-jinja2 python-six python-yaml python3-pip python3-setuptools golang

go get git.autistici.org/ale/x509ca
go get git.autistici.og/ale/ed25519gen
pip3 install wheel
pip3 install ansible jmespath netaddr
# optionally install https://mitogen.networkgenomics.com/ (if not, uncomment it in float-minimal/ansible.conf

```


#### generate ssl ca / ssh ca / passwords and copy them to the hosts

```
../float/float run init-credentials
```

This will create /etc/credentials

Which credentials are on which hosts is defined in services.yml

#### generate hiera data + manifests from hosts.yml and run puppet on all hosts

```
../float/float run puppet
```

This will generate a hiera tree + manifest in /etc/puppet/hiera/[common.json|common.yml|hiera|host.config.yml|host_manifest.pp] on each host defined in hosts.yml.

You can assign puppet roles in hosts.yml, the templates are in float/roles/puppetapply/templates , and the code that generates vms and networks is in: plugins/action/gen_hiera.py

To test stuff with that hiera data / module path you can use /usr/local/bin/puppet.sh


