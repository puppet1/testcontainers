#!/usr/bin/env bash
echo "" | ssh-keygen -t ed25519 -f admin -C testkey -P ''
cp admin.pub authorized_keys.docker
cat my.key >> authorized_keys.docker
echo "" | ssh-keygen -t ed25519 -f hv1 -C testkey -P ''
echo "" | ssh-keygen -t ed25519 -f hv2 -C testkey -P ''
echo "" | ssh-keygen -t ed25519 -f hv3 -C testkey -P ''
