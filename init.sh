#!/usr/bin/env bash

set -eux

docker-compose down

cd sshkeys
./genkeys.sh || true
cd ..

docker network rm ext || true
docker network create \
  --driver=bridge \
  --subnet=172.28.5.0/24 \
  --ip-range=172.28.5.0/24 \
  --gateway=172.28.5.1 \
  ext
# --ip-masq=true \
#  --aux-address="my-router=192.168.10.5" --aux-address="my-switch=192.168.10.6" \

docker-compose up --build
