#!/usr/bin/env bash

set -x

h="root@192.168.56.104"

./reverse-sshfs $h /c/puppet/code/ /etc/puppet/code &
./reverse-sshfs $h /c/puppet/private_modules/ /etc/puppet/private_modules &
./reverse-sshfs $h /c/puppet/scripts/ /etc/puppet/scripts &
